<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">

<head>
<base href="<%=basePath%>">
  <meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">

  <title>停车中心管理系统</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" >
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3" style="color: white;">控制台 <sup>@</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" >
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>车辆中心管理</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <li class="nav-item">
        <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>功能开发中...</span>
        </a>
        
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>功能开发中...</span>
        </a>
        
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
       <li class="nav-item active">
        <a class="nav-link" >
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>人员中心管理</span></a>
      </li>
      <hr class="sidebar-divider">

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed"  data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>功能开发中...</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
         
        </div>
      </li>

      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" >
          <i class="fas fa-fw fa-chart-area"></i>
          <span>功能开发中...</span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" >
          <i class="fas fa-fw fa-table"></i>
          <span>功能开发中</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>
            <!-- 获取到username值，放进去 -->
            <!-- 获取到username值，放进去 -->
            <!-- 获取到username值，放进去 -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle"  id="userDropdown"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">您好，${usergame}</span>
                
              </a>
            </li>
          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">数据浏览中心</h1>
            <a href="index.jsp" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">退出登录</a>
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">本月收入（更换月份）</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">RMB：40,000</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">已占位（添加正在维护车位）</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">80辆</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">剩余停车位（目前车位总量：150）</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">70辆</div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">本日计费规则（更改）</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">5rmb/h</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">最近数据</h6><font color="bule">${msgd }</font>
                  <div class="dropdown no-arrow">
                  
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                     	
                    </div>
                  </div>
                  
                </div>
               <!-- 表格写出车位列表 --> 
            		    
                	 <table border="3" width="1050x" style="border-color: green;font-size: 14px">
     					 <tr>
       					        <th>车牌号</th>
        				     	<th>登记姓名</th>
     					  	    <th>驶入时间</th>
        				    	<th>驶出时间</th>
       						    <th>总金额</th>
       						    <th>操作</th>
       						    
     					 </tr>
     					 <c:forEach items="${list}" var="li" >
        						<tr>
        						<form action="" method="post">
         							 <td>${li.carnub }</td>
         							 <td>${li.cardriname }</td>
         							 <td>${li.carstartTime }</td>
         							 <td>${li.carfinishTime }</td>
         							 <td>${li.cartotalMoney }</td>   							 	
         							 <!--<td><a href="<%=request.getContextPath()%>/servlet/DeleteServlet?cn=${li.carnub}&cst=${li.carstartTime }" method="post" >删除</a></td>  -->
         							 <td style="width: 130px">
         							 	
         							 	<input formaction="<%=request.getContextPath()%>/servlet/DeleteServlet?cn=${li.carnub}&cst=${li.carstartTime }" type="submit" value="删除" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
         							 	<input 
         							 	formaction="<%=request.getContextPath()%>/servlet/UpdateReadyServlet?one=${li.carnub}&two=${li.cardriname }&three=${li.carstartTime }&fore=${li.carfinishTime }&five=${li.cartotalMoney }" 
         							 	type="submit"  value="修改" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
         							 	
         							 	</form>
         							 </td>
        						</tr>
     					 </c:forEach>
    </table>
                	
                
                <div class="card-body">
                
                  <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                    
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">添加新车辆</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    
                    </a>
                    
                  </div>
                </div>
                <!-- Card Body -->
                <!-- 添加新车辆信息 -->
           				
               
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
            <form action="servlet/AddServlet" method="post">
								<p>驾驶人基础信息&nbsp;&nbsp;&nbsp;
								<input type="text" placeholder="“车牌号”"  name="chepaihao">&nbsp;&nbsp;&nbsp;
								<input type="text" placeholder=“登记姓名” name="xingming" style="width: 145px"></p>
							&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;&nbsp;驶入时间&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="text" placeholder=“驶入时间” value="2020-3-5 00:00:00" name="shirushijian" style="width:auto "></p>
										&nbsp;&nbsp;&nbsp;&nbsp;注意格式为:2023-3-5 00:00:00
										<br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<span style="color: red">${qwe }</span><br>
										
										<input type="submit"style="width: 155px" value="新增" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" >

	    	</form>	
                  </div>
                  
                  <div class="mt-4 text-center small">
                    <span class="mr-2">
                      <i class="fas fa-circle text-primary"></i>
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-success"></i> 
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-info"></i>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          
</body>

</html>
