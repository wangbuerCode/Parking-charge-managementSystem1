<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'update.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	 <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <style type="text/css">



.div1{
            width: auto;
            height: 600px;
            background: #cccccc;
            position: relative;
        }
.div2{
            position: absolute;
            left: 50%;
            top: 50%;
            margin-left: -600px;
            margin-top: -200px;
            width: 1200px;
            height: 400px;
            background: #4fcc8d;
            line-height: 400px;
            font-size: 36px;
            text-align: center;
        }

</style>

  </head>
  
  <body class="div1">

    <div class="col-xl-4 col-lg-5" style="padding-left: 100x">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">修改订单</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    
                    </a>
                    
                  </div>
                </div>
                <!-- Card Body -->
                <!-- 修改新车辆信息 -->
           				
               
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
            <form action="servlet/UpdateOrderServlet" method="post">
								<p>驾驶人基础信息&nbsp;&nbsp;&nbsp;
								<input type="text" placeholder="${one }" readonly="readonly" defaultValue=${one } value="${one }"  name="one"  >&nbsp;&nbsp;&nbsp;
								<input type="text" placeholder="${two }" readonly="readonly"   value="${two }" name="two" style="width: 145px"></p>
							&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;驶入时间&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="text" placeholder="${three }" readonly="readonly"   defaultValue=${three }  value="${three }" name="three" ></p>
										注意格式为:2020-3-5 00:00:00
										
										<br><br>
							&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;驶出时间&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="text"  value="${fore }"  name="fore" style="width: "></p>
										注意格式为:2020-3-5 00:00:00
										<br>
										
										<br>
								<p>共需支付钱数&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" placeholder="${five }" value="${five }" defaultValue=${five }  name="five">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;现需手动计算
										<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<br><br><br><span style="color: red">${errsmg }</span><br><br><br>
								
								
								
								
								<input type="submit"style="width: 155px" value="更改" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" >	
								<input type="submit" formaction="<%=request.getContextPath()%>/servlet/MainServlet" style="width: 155px" value="返回上一级" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" >
	   			 
	    	</form>	
                  </div>
                   <div class="mt-4 text-center small">
                    <span class="mr-2">
                    <br>
                     
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-success"></i> 
                    </span>
                    <span class="mr-2">
                      
                     <br><br><br><br><br><br><br><br><br><i class="fas fa-circle text-info"></i> 驶入信息 <i class="fas fa-circle text-primary"></i> 更新数据<br>
                    </span>
                  </div>
                 
                </div>
                 <br><br><br><br>	
                
                </div>
                
                
  </body>
</html>
