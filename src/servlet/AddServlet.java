package servlet;

import java.io.IOException;
import java.io.PrintWriter;


import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.impl.CarServiceImpl;
import dao.impl.CarDaoImpl;
import entity.Car;
import entity.DateUtil;

public class AddServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		CarServiceImpl csi=new CarServiceImpl();

		System.out.println("新增servlet");
		String chepaihao=request.getParameter("chepaihao");//获取
		String xingming=request.getParameter("xingming");
		String shirushijian1=request.getParameter("shirushijian");
		
		if(chepaihao==""||xingming==""||shirushijian1==""){
			String err="你有为未填完的数据";//反馈
			request.getSession().setAttribute("qwe",err);//去给另一个servlet传值
			request.getRequestDispatcher("MainServlet").forward(request, response);//进入main界面serlvet
			
		}else{
//			System.out.println("更新前--"+shirushijian1);
			Date shirushijian=DateUtil.convertStringToDate(shirushijian1);
//			Date a=DateUtil.convertStringToDate(shirushijian1);
//			System.out.println("更新后--"+a);
//			System.out.println("再次编译后--"+DateUtil.converDateToString(a));
			Car car=new Car();
			car.setCardriname(xingming);
			car.setCarnub(chepaihao);
			car.setCarstartTime(shirushijian);
			int i=csi.addCarOder(car);
			System.out.println(i);
			String msgd="最近已添加数据<"+chepaihao+">,驶入时间为<"+shirushijian1+">的信息。";//反馈
			request.getSession().setAttribute("msgd",msgd);//去给另一个servlet传值
			request.getRequestDispatcher("MainServlet").forward(request, response);//进入main界面serlvet
			
		}

		
		
		
		
	}

}
