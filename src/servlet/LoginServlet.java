package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.impl.UserServiceImpl;
import dao.impl.UserDaoImpl;

public class LoginServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		System.out.println("进入loginservlet");
		
		UserServiceImpl udi=new UserServiceImpl();//获取方法
		
		
		
		
		
		//从表单获取用户名和密码进行数据库对比
		String username=request.getParameter("username");//获取用户名
		String userpwd=request.getParameter("userpwd");//获取密码
		System.out.println(username+"----"+userpwd);
		if(username==""||userpwd==""){
			request.setAttribute("error", "用户名或密码为空!");//如果密码出错，传值给前台
			request.getRequestDispatcher("/index.jsp").forward(request, response);//在返回给登录界面
		}else if(udi.login(username, userpwd)!=null){
			request.getSession().setAttribute("uuname",username);//去给另一个servlet传值
			request.getRequestDispatcher("MainServlet").forward(request, response);//进入主界面serlvet
			//response.sendRedirect("MainServlet");
			
		}else{
			request.setAttribute("error", "用户名或密码错误！！！");//如果密码出错，传值给前台
			request.getRequestDispatcher("/index.jsp").forward(request, response);//在返回给登录界面
		}
		
		
	}

}
