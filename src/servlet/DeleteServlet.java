package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.impl.CarDaoImpl;

public class DeleteServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		CarDaoImpl cdi=new CarDaoImpl();
		
		System.out.println("删除servlet1");
		String cn=request.getParameter("cn");//获取用户名
		String cst=request.getParameter("cst");
		int i=cdi.deleteCarOrder(cn, cst);
		System.out.println(i);
		String msgd="最近已删除数据<"+cn+">,驶入时间为<"+cst+">的信息。";//删除反馈
		request.getSession().setAttribute("msgd",msgd);//去给另一个servlet传值
		request.getRequestDispatcher("MainServlet").forward(request, response);//进入main界面serlvet
		
	}

}
