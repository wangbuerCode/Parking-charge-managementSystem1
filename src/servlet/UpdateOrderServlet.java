package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.impl.CarServiceImpl;
import entity.Car;
import entity.DateUtil;

public class UpdateOrderServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		System.out.println("orderupdate");
		CarServiceImpl csi=new CarServiceImpl();
		//获取各集合
		String one=request.getParameter("one");//获取用户名
		String two=request.getParameter("two");
		String three=request.getParameter("three");
		String fore=request.getParameter("fore");
		String five=request.getParameter("five");
		System.out.println("1:"+one);
		System.out.println("2:"+two);
		System.out.println("3:"+three);
		System.out.println("4:"+fore);
		System.out.println("5:"+five);
		
		if(two==""||fore==""){
			request.setAttribute("errsmg","请检查是否填写完整" );//这三部获取user对象值传到main界面显示
			request.getRequestDispatcher("/update.jsp").forward(request, response);//进入更新界面
			
		}else{
			Car car=new Car();
			car.setCarnub(one);
			car.setCardriname(two);
			car.setCarstartTime(DateUtil.convertStringToDate(three));
			car.setCarfinishTime(DateUtil.convertStringToDate(fore));
			//System.out.println("时间"+DateUtil.convertStringToDate(fore));
			car.setCartotalMoney(Double.valueOf(five));
			
			csi.updateCarOder(car);
			
			String msgd="最近已修改一条数据<"+one+">,驶入时间为<"+three+">的信息。";//删除反馈
			request.getSession().setAttribute("msgd",msgd);//去给另一个servlet传值
			request.getRequestDispatcher("MainServlet").forward(request, response);//进入main界面serlvet
			
		}
		
		
	}

}
