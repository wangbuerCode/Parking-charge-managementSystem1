package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.impl.CarServiceImpl;

public class UpdateReadyServlet extends HttpServlet {


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		System.out.println("更新servlet");
		//获取各集合
		String one=request.getParameter("one");//获取用户名
		String two=request.getParameter("two");
		String three=request.getParameter("three");
		String fore=request.getParameter("fore");
		String five=request.getParameter("five");
//		System.out.println(one);
//		System.out.println(two);
//		System.out.println(three);
//		System.out.println(fore);
//		System.out.println(five);
		
		request.getSession().setAttribute("one",one);//去给另一个servlet传值
		request.getSession().setAttribute("two",two);
		request.getSession().setAttribute("three",three);
		request.getSession().setAttribute("fore",fore);
		request.getSession().setAttribute("five",five);
		
		request.getRequestDispatcher("/update.jsp").forward(request, response);//进入更新界面
		
			
	}

}
