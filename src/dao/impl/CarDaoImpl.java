package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.sql.Statement;

import dao.BaseDao;
import dao.CarDao;
import entity.Car;
import entity.DateUtil;
import entity.User;




public class CarDaoImpl extends BaseDao implements CarDao {
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	Statement st=null;
	//
	

	public int getMoneyisNull() {
		// TODO Auto-generated method stub
		return 0;
	}
	//实现方法

	public int addCarOder(Car car) {
		String starttime=DateUtil.converDateToString(car.getCarstartTime());
		System.out.println("dao层的时间为"+starttime);
		String sql="INSERT INTO car_data (carnub,cardriname,carstartTime) VALUES "+ "('"+car.getCarnub()+"','"+car.getCardriname()+"','"+starttime+"')";
		System.out.println("新增语句---"+sql);
		int i = 0;
		 try {
	            //获得连接
			 con=this.getCon();
	            //获得语句执行者
			 	con = this.getCon();
				st=con.createStatement();
				i=st.executeUpdate(sql);
	        } catch (Exception e) {
	            throw new RuntimeException(e);

	        } finally {
	        	this.closeAll(con, ps, rs);
	        }
		return i;
	}
	
	//更新
	public int updateCarOder(Car car) {
		String starttime=DateUtil.converDateToString(car.getCarstartTime());
		String endtime=DateUtil.converDateToString(car.getCarfinishTime());
		String sql="update car_data set cardriname='"+car.getCardriname()+"' , carfinishTime='"+endtime+"' , cartotalMoney='"+car.getCartotalMoney()+"' where carnub='"+car.getCarnub()+"' and carstartTime='"+starttime+"'";
		System.out.println("更新语句---"+sql);
		int i = 0;
		 try {
	            //获得连接
			 con=this.getCon();
	            //获得语句执行者
			 	con = this.getCon();
				st=con.createStatement();
				i=st.executeUpdate(sql);
	        } catch (Exception e) {
	            throw new RuntimeException(e);

	        } finally {
	        	this.closeAll(con, ps, rs);
	        }
		return i;
	}
	//获取无参
	public ArrayList<Car> getAll() {
		ArrayList<Car> list=new ArrayList<Car>();
		
		String sql="select * from car_data ";
		try {
			con=this.getCon();
			ps = con.prepareStatement(sql);
			rs=ps.executeQuery();
				while(rs.next()){
					Car car=new Car(rs.getString(1), rs.getString(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getDouble(5));
					
					list.add(car);
				}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeAll(con, ps, rs);
		}

		return list;
	}

	public int deleteCarOrder(String carnub, String carstartTime) {
		String sql="Delete from car_data where carnub='"+carnub+"' and carstartTime='"+carstartTime+"'";
		System.out.println("删除语句---"+sql);
		int i = 0;
		try {
			con = this.getCon();
			st=con.createStatement();
			i=st.executeUpdate(sql);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeAll(con, ps, rs);
		}
		return i;
	}
	
}
