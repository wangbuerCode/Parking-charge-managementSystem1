package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.BaseDao;
import dao.UserDao;
import entity.User;
	//此类是实现数据库互通层
public class UserDaoImpl extends BaseDao implements UserDao {
	
	
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	
	
	//登录方法，返回值为一个user对象便于显示个人信息
	public User login(String username, String userpwd) {
		User user=null;
		String sql="select * from car_user where userid=? and userpwd=?";
		try {
			con = this.getCon();
			ps = con.prepareStatement(sql);
			
			ps.setString(1, username);//通过索引赋值
			ps.setString(2, userpwd);
			ResultSet rs=ps.executeQuery();//方法
			if(rs.next()){
				user=new User();
				
				user.setUsername(username);
				
				user.setUserpwd(userpwd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			this.closeAll(con, ps, rs);
		}
		return user;
	}

}
