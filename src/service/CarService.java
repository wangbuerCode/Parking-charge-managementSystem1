package service;

import java.util.ArrayList;
import java.util.Date;

import entity.Car;
//车辆服务层实现接口
public interface CarService {
		//获取所有，不带条件
			public ArrayList<Car> getAll();
			
			//取得条数，条件为价格为空
			public int getMoneyisNull();
			
			//插入一条数据
			public int addCarOder(Car car);
			
			//更新一条数据
			public int updateCarOder(Car car);
			
			public int deleteCarOrder(String carnub,String carstartTime);
}
