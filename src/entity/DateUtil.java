package entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	/**
	 * String -->Date
	 * @return
	 */
	public static Date convertStringToDate(String time){
		Date date=null;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {
			date=sdf.parse(time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return date;
	}
	
	public static String converDateToString(Date date){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		 return sdf.format(date);
	}

		/*修改格式的*/
	public static String mydemofunction(String time){
		Date date=convertStringToDate(time);
		time=converDateToString(date);
		
		return time;
	}
	
	/*获取时间差，格式yyyy-MM-dd hh:mm:ss*/
	public static int getDatePoor(Date nowDate, Date oldDate) {
		
	    long nd = 1000 * 24 * 60 * 60;
	    long nh = 1000 * 60 * 60;
	    long nm = 1000 * 60;
	    // long ns = 1000;
	    // 获得两个时间的毫秒时间差异
	    long diff = nowDate.getTime() - oldDate.getTime();
	    // 计算差多少天
	    int day = (int) (diff / nd);
	    // 计算差多少小时
	    long hour = diff % nd / nh;
	    // 计算差多少分钟
	    long min = diff % nd % nh / nm;
	    // 计算差多少秒//输出结果
	    // long sec = diff % nd % nh % nm / ns;
	    if(hour>00 ||min>00){
	    	day+=1;
	    }
	    return day;
	}
	
	public static  String getNow(){
		Date day=new Date();    

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 

		String date =df.format(day);
		return date;
	}
}
