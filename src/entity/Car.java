package entity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Car {

	//carnub,cardriname,carstartTime,carfinishTime,cartotalMoney
	private String carnub;
	private String cardriname;
	private Date carstartTime;
	private Date carfinishTime;
	private Double cartotalMoney;
	public String getCarnub() {
		return carnub;
	}
	public void setCarnub(String carnub) {
		this.carnub = carnub;
	}
	public String getCardriname() {
		return cardriname;
	}
	public void setCardriname(String cardriname) {
		this.cardriname = cardriname;
	}
	public Date getCarstartTime() {
		
		return carstartTime;
	}
	public void setCarstartTime(Date carstartTime) {
		this.carstartTime = carstartTime;
	}
	public Date getCarfinishTime() {
		return carfinishTime;
	}
	public void setCarfinishTime(Date carfinishTime) {
		this.carfinishTime = carfinishTime;
	}
	public Double getCartotalMoney() {
		return cartotalMoney;
	}
	public void setCartotalMoney(Double cartotalMoney) {
		this.cartotalMoney = cartotalMoney;
	}
	@Override
	public String toString() {
		return "Car [carnub=" + carnub + ", cardriname=" + cardriname
				+ ", carstartTime=" + carstartTime + ", carfinishTime="
				+ carfinishTime + ", cartotalMoney=" + cartotalMoney + "]";
	}
	public Car(String carnub, String cardriname,Date carstartTime,Date carfinishTime,Double cartotalMoney) {
		super();
		this.carnub=carnub;
		this.cardriname=cardriname;
		this.carstartTime=carstartTime;
		this.carfinishTime=carfinishTime;
		this.cartotalMoney=cartotalMoney;
		
	}
	public Car(){super();}
	
}
