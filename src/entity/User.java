package entity;

public class User {
	private String username;
	private String userpwd;
	public String getUsername() {
		return username;
	}
	public String getUserpwd() {
		return userpwd;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setUserpwd(String userpwd) {
		this.userpwd = userpwd;
	}
	public User(String username,String userpwd) {
		super();
		this.username=username;
		this.userpwd=userpwd;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
}
